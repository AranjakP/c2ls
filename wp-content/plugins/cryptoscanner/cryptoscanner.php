<?php

/*
  Plugin Name: Cryptoscanner
  Description: Cryptoscanner handler
  Version: 0.1
  Author: KPS
 */

function kps_cryptoscanner() {
    ob_start();
    load_template(dirname(__FILE__) . '/templates/cryptoscanner_template.php');
    echo ob_get_clean();
}

add_shortcode('cryptoscanner', 'kps_cryptoscanner');

add_action('wp_ajax_kps_get_signals', 'kps_get_signals');
add_action('wp_ajax_nopriv_kps_get_signals', 'kps_get_signals');
function kps_get_signals() {
//    $return = array(
//        'message' => 'Сохранено',
//        'ID' => 1
//        
//    );   
    
    global $wpdb;
    $sql = 'SELECT cs.*, co.logo_url FROM wp_cryptoscanner cs '
			. 'LEFT JOIN wp_coins co ON co.pair = cs.pair '
            . 'WHERE '
            . 'cs.exchange IN ("' . implode('","',explode(',',$_POST['exchanges'])) . '") AND '
			. ($_POST['bitcoin']==1?'cs.bitcoin= ' . $_POST['bitcoin'] . ' AND ':'')
			. ($_POST['bollinger']==1?'cs.bollinger= ' . $_POST['bollinger'] . ' AND ':'')
			. ($_POST['rsi']==1?'cs.rsi= ' . $_POST['rsi'] . ' AND ':'')
			. ($_POST['stochastic']==1?'cs.stochastic= ' . $_POST['stochastic'] . ' AND ':'')
            . 'cs.profit >= ' . $_POST['minProfit'] . ' AND '
            . 'cs.volume >= ' . $_POST['minVolume'];
    $results = $wpdb->get_results($sql);
    
    wp_send_json_success($results);

}

function kps_cryptoscanner_history() {
    ob_start();
    load_template(dirname(__FILE__) . '/templates/cryptoscanner_history_template.php');
    echo ob_get_clean();
}

add_shortcode('cryptoscanner_history', 'kps_cryptoscanner_history');

function kps_cryptoscanner_statistic() {
    ob_start();
    load_template(dirname(__FILE__) . '/templates/cryptoscanner_statistic_template.php');
    echo ob_get_clean();
}

add_shortcode('cryptoscanner_statistic', 'kps_cryptoscanner_statistic');

//add_shortcode('cryptoscanner_history', 'kps_cryptoscanner_history');

function kps_cryptoscanner_statistic_details() {
    ob_start();
    load_template(dirname(__FILE__) . '/templates/cryptoscanner_statistic_details_template.php');
    echo ob_get_clean();
}

add_shortcode('cryptoscanner_statistic_details', 'kps_cryptoscanner_statistic_details');

function kps_cryptoscanner_users_panel() {
    ob_start();
    load_template(dirname(__FILE__) . '/templates/cryptoscanner_users_panel.php');
    echo ob_get_clean();
}

add_shortcode('cryptoscanner_users_panel', 'kps_cryptoscanner_users_panel');

add_action('kps_scheduled_clean_history', 'kps_scheduled_clean_history');
function kps_scheduled_clean_history() {
	global $wpdb;
	$sql2 = "SELECT * 
			FROM (SELECT pair, COUNT(1) qty
						FROM wp_cryptoscanner_history cs
                  		WHERE status = 'Active'
						GROUP BY 1) as total
			WHERE total.qty > 1 AND pair != 'BTC_USDT' AND pair != 'USD-BTC'";
			

	$results2 = $wpdb->get_results($sql2);
	
	foreach ($results2 as $key => $value) {
		$sql3 = "SELECT *
					FROM wp_cryptoscanner_history cs
					WHERE status = 'Active' AND pair = '".$value->pair."'";
			

		$results3 = $wpdb->get_results($sql3);

		$ids = array();
		foreach ($results3 as $key1 => $value1) {
			$ids[] = $value1->ID;
			$sql4 = "DELETE FROM wp_cryptoscanner_history 
					WHERE ID NOT IN (". implode(",", $ids).")"
					. " AND pair='".$value1->pair."'"
					. " AND bollinger=".$value1->bollinger
					. " AND rsi=".$value1->rsi
					. " AND stochastic=".$value1->stochastic
					. " AND buy=".$value1->buy
					. " AND sell=".$value1->sell
					. " AND stop=".$value1->stop
					. " AND profit=".$value1->profit
					. " AND status = 'Active'"
					. " AND TIMESTAMPDIFF(MINUTE,'".$value1->date."',date) < 4320";

			$results4 = $wpdb->get_results($sql4);
		}
	}
	
	$sql21 = "SELECT * 
			FROM (SELECT pair, COUNT(1) qty
						FROM wp_cryptoscanner_history cs
                  		WHERE status = 'In Progress'
						GROUP BY 1) as total
			WHERE total.qty > 1 AND pair != 'BTC_USDT' AND pair != 'USD-BTC'";
			

	$results21 = $wpdb->get_results($sql21);
	
	foreach ($results21 as $key21 => $value21) {
		$sql31 = "SELECT *
					FROM wp_cryptoscanner_history cs
					WHERE status = 'In Progress' AND pair = '".$value21->pair."'";
			

		$results31 = $wpdb->get_results($sql31);

		$ids21 = array();
		foreach ($results31 as $key31 => $value31) {
			$ids21[] = $value31->ID;
			$sql41 = "DELETE FROM wp_cryptoscanner_history 
					WHERE ID NOT IN (". implode(",", $ids21).")"
					. " AND pair='".$value31->pair."'"
					. " AND bollinger=".$value31->bollinger
					. " AND rsi=".$value31->rsi
					. " AND stochastic=".$value31->stochastic
					. " AND buy=".$value31->buy
					. " AND sell=".$value31->sell
					. " AND stop=".$value31->stop
					. " AND profit=".$value31->profit
					. " AND status = 'In Progress'"
					. " AND TIMESTAMPDIFF(MINUTE,'".$value31->date."',date) < 4320";

			$results41 = $wpdb->get_results($sql41);
		}
	}
	
		/****Coin stat****/
		$sql = 'SELECT cs.pair, '
				. 'sum(case when status = "StopLoss" then 1 else 0 end) SLCount, '
				. 'sum(case when status = "TakeProfit" then 1 else 0 end) TPCount '
				. 'FROM wp_cryptoscanner_history cs '
				. 'WHERE cs.profit > 0 AND status != "Unknown" '
				. 'GROUP BY 1';

		$results = $wpdb->get_results($sql);

		foreach ($results as $key => $row) {
			$totalCount = $row->TPCount + $row->SLCount;
			if ($totalCount > 0 ) {
				$tpPercantage = number_format($row->TPCount/$totalCount*100,2);
				$sql5 = 'UPDATE wp_cryptoscanner SET profit_stat="'.$tpPercantage.'% ('.$row->TPCount.'/'.$totalCount.')" WHERE pair="'.$row->pair.'"';
				$wpdb->get_results($sql5);
			} else {
				$sql5 = 'UPDATE wp_cryptoscanner SET profit_stat="Unknown" WHERE pair="'.$row->pair.'"';
				$wpdb->get_results($sql5);
			}
		}
		
		/****Close Active to Not fulfilled****/
		$sql8 = "SELECT * FROM wp_cryptoscanner "
				. "WHERE pair IN ("
				. "SELECT pair
				FROM wp_cryptoscanner_history cs
                WHERE status = 'Active' AND pair != 'BTC_USDT' AND pair != 'USD-BTC')";
		$results8 = $wpdb->get_results($sql8);
		$coins = array();
		foreach ($results8 as $key8 => $row8) {
			$coins[$row8->pair] = array('buy'=>$row8->buy,'sell'=>$row8->sell,'stop'=>$row8->stop);
		}		
		
		$sql7 = "SELECT ID, pair, buy, sell, stop 
				FROM wp_cryptoscanner_history cs
                WHERE status = 'Active' AND pair != 'BTC_USDT' AND pair != 'USD-BTC'";
		$results7 = $wpdb->get_results($sql7);

		foreach ($results7 as $key7 => $row7) {
			if (array_key_exists($row7->pair,$coins)) {
				if ($coins[$row7->pair]['buy'] != $row7->buy || 
					$coins[$row7->pair]['sell'] != $row7->sell || 
					$coins[$row7->pair]['stop'] != $row7->stop) {
					$sql9 = 'UPDATE wp_cryptoscanner_history SET status ="Not Fulfilled" WHERE ID ="'.$row7->ID.'"';
					$wpdb->get_results($sql9);
				}
			}
		}
}

add_action('wp_ajax_kps_create_order', 'kps_create_order');
//add_action('wp_ajax_nopriv_kps_create_order', 'kps_create_order');
function kps_create_order() {
	$user_id = get_current_user_id();
	$expired_date = get_the_author_meta('kps_cryptoscanner_buy_expired_date', $user_id);

	if (!check_expired_date($expired_date)) {
		$results = array(
			'message' => 'Your access to Buy functionality is expired' . ($expired_date?" on ".$expired_date:'!'),
			'cs_status' => 'error'
		);
	} else {
		global $wpdb;

		$sql = 'INSERT INTO wp_cryptoscanner_orders'
				. '(user_id, exchange, pair, order_type, buy, sell, stop, profit, order_status) '
				. 'VALUES ("' . $user_id . '","' . $_POST['exchange'] . '","' . $_POST['pair'] . '","' . $_POST['order_type'] . '","' . $_POST['buy'] . '","' . $_POST['sell'] . '","' . $_POST['stop'] . '","' . $_POST['profit'] . '","Active")';
		$wpdb->get_results($sql);

		$results = array(
			'message' => 'But now it is test mode, any order will not be created on exchange.',
			'cs_status' => 'ok'
		);
	}

	wp_send_json_success($results);
}

function check_expired_date($expired_date) {
	$expired_datetime = strtotime($expired_date);
	if ($expired_datetime < time()) {
		return false;
	}
	return true;
}

add_action('wp_ajax_kps_update_uls', 'kps_update_uls');

function kps_update_uls() {
	$user_id = get_current_user_id();
	update_user_meta( $user_id, 'when_last_login', time() );

	wp_send_json_success();
}