/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.setInterval(updateSignals, 5000);
window.setInterval(updateUserLastSeen, 600000);

jQuery(document).on('change', '#bollinger, #rsi, #stochastic, #minProfit, #minVolume, #btc, #bittrex, #binance', function () {
	updateSignals();
});

jQuery(window).load(function () {
	updateSignals();
	updateUserLastSeen();
});

function updateSignals() {
	jQuery('.signal_row').addClass('removable');
	var exchanges = jQuery("input[name='exchanges']:checked").map(function () {
		return jQuery(this).val();
	}).get().join(",");
	data = {
		action: 'kps_get_signals',
		btc: (jQuery('#btc').is(":checked") ? 1 : 0),
		bollinger: (jQuery('#bollinger').is(":checked") ? 1 : 0),
		rsi: (jQuery("#rsi").is(":checked") ? 1 : 0),
		stochastic: jQuery('#stochastic').is(":checked") ? 1 : 0,
		minVolume: jQuery("#minVolume").val(),
		minProfit: jQuery("#minProfit").val(),
		exchanges: exchanges
	};

	jQuery.ajax({
		type: "POST",
		url: "/wp-admin/admin-ajax.php",
		data: data,
		success: successHandler,
		dataType: 'json'
	});
}

function successHandler(data, textStatus) {
//	console.log(data.data);
	notify = false;

	jQuery.each(data.data, function (idx, obj) {
		if (!jQuery('#' + obj.pair).length) {
			item = jQuery('#signal_template').clone();
			item.attr("id", obj.pair);
			item.removeClass('hidden').addClass('signal_row');
			item.find('.market').text(obj.pair);
			item.find('.price').text(obj.price);
			item.find('.bollinger').text(obj.bollinger === '1' ? 'YES' : '-');
			obj.bollinger === '1' ? item.find('.bollinger').addClass('alert-success') : item.find('.bollinger').removeClass('alert-success');
			item.find('.rsi').text(obj.rsi === '1' ? 'YES' : '-');
			obj.rsi === '1' ? item.find('.rsi').addClass('alert-success') : item.find('.rsi').removeClass('alert-success');
			item.find('.stochastic').text(obj.stochastic === '1' ? 'YES' : '-');
			obj.stochastic === '1' ? item.find('.stochastic').addClass('alert-success') : item.find('.stochastic').removeClass('alert-success');
			item.find('.buy').text(obj.buy);
			item.find('.sell').text(obj.sell);
			item.find('.profit').text(parseFloat(obj.profit).toFixed(2) + '%');
			item.find('.stop').text(obj.stop);
			item.find('.volume').text(parseFloat(obj.volume).toFixed(2));
			item.find('#logo_url').attr("src", obj.logo_url);
			item.find('#market_link').text(obj.exchange);
			item.find('#market_link').attr("href", obj.link);
			item.find('#profit_stat').text(obj.profit_stat);
			item.find('#profit_stat').attr("href",'cryptoscanner-statistic-details/?pair='+obj.pair);
			jQuery('#signals').append(item);
			notify = true;

		} else {
			item = jQuery('#' + obj.pair);
			item.removeClass('removable');
			item.find('.price').text(obj.price);
			item.find('.bollinger').text(obj.bollinger === '1' ? 'YES' : '-');
			obj.bollinger === '1' ? item.find('.bollinger').addClass('alert-success') : item.find('.bollinger').removeClass('alert-success');
			item.find('.rsi').text(obj.rsi === '1' ? 'YES' : '-');
			obj.rsi === '1' ? item.find('.rsi').addClass('alert-success') : item.find('.rsi').removeClass('alert-success');
			item.find('.stochastic').text(obj.stochastic === '1' ? 'YES' : '-');
			obj.stochastic === '1' ? item.find('.stochastic').addClass('alert-success') : item.find('.stochastic').removeClass('alert-success');
			item.find('.buy').text(obj.buy);
			item.find('.sell').text(obj.sell);
			item.find('.profit').text(parseFloat(obj.profit).toFixed(2) + '%');
			item.find('.stop').text(obj.stop);
			item.find('.volume').text(parseFloat(obj.volume).toFixed(2));
			item.find('#profit_stat').text(obj.profit_stat);
			item.find('#profit_stat').attr("href",'cryptoscanner-statistic-details/?pair='+obj.pair);
		}
	});

	jQuery('.removable').remove();

	if (notify) {
		notifyUser();
	}
}

function notifyUser() {
	var options = {
		title: "Signals updated",
		options: {
			body: "Check scanner, please.",
			icon: "/wp-content/plugins/cryptoscanner/templates/assets/cryptotrade.jpg",
			lang: 'en-US'//,
		}
	};
	jQuery("#easyNotify").easyNotify(options);
	var audio = new Audio('/wp-content/plugins/cryptoscanner/templates/assets/profit.wav');
	audio.play();
}

jQuery(document).on('click', '.buy_signal', function (e) {
	e.preventDefault();
	if(!jQuery('body').hasClass('logged-in')){
		alert('Your should be logged in to use this functioanality!');
		return;
	}
	signal = jQuery(this).closest('.signal_row');
	data = {
		action: 'kps_create_order',
		buy: signal.find('.buy').text(),
		sell: signal.find('.sell').text(),
		stop: signal.find('.stop').text(),
		profit: signal.find('.profit').text(),
		pair: signal.find('.market').text(),
		exchange: signal.find('#market_link').text(),
		order_type: jQuery(this).hasClass("buy_now") ? 1 : 0
	};
	
	jQuery.ajax({
		type: "POST",
		url: "/wp-admin/admin-ajax.php",
		data: data,
		success: successHandlerBuy,
		dataType: 'json'
	});
});

function successHandlerBuy(data, textStatus) {
	var title = "Order added in queue";
	if (data.data.cs_status == "error") {
		title = "Warning!!!";
	}
	var body = data.data.message;
	
	var options = {
		title: title,
		options: {
			body: body,
			icon: "/wp-content/plugins/cryptoscanner/templates/assets/cryptotrade.jpg",
			lang: 'en-US'
		}
	};
	jQuery("#easyNotify").easyNotify(options);
}

function updateUserLastSeen() {
	data = {
		action: 'kps_update_uls'		
	};

	jQuery.ajax({
		type: "POST",
		url: "/wp-admin/admin-ajax.php",
		data: data
	});
}
