<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
if ($_POST) {
	if ($_POST['user_id']) {
		update_user_meta($_POST['user_id'], 'kps_cryptoscanner_expired_date', $_POST['cryptoscanner_expired_date']);
		update_user_meta($_POST['user_id'], 'kps_cryptoscanner_buy_expired_date', $_POST['cryptoscanner_buy_expired_date']);
	}
}

wp_register_style('cryptoscanner_css', plugin_dir_url(__FILE__) . 'assets/cryptoscanner.css');
wp_enqueue_style('cryptoscanner_css');
wp_enqueue_script('jquery-ui-datepicker');
wp_register_style('jquery-ui-style', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css?ver=1.11.4', array(), $jquery_version);
wp_enqueue_style('jquery-ui-style');
$current_user_id = get_current_user_id();
if (!($current_user_id == 1 || $current_user_id == 8)) {
	?>
	<div class="container">
		<div class="alert alert-danger text-center">
			<strong>Attention!</strong> Access is denied!
		</div>
	</div>
	<?php
} else {
	?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		jQuery(function ($) {
			$(".cryptoscanner_expired_date").datepicker({dateFormat: 'MM d, yy'});
			$(".cryptoscanner_buy_expired_date").datepicker({dateFormat: 'MM d, yy'});
		});
	</script>
	<div class="container">
		<div id="users">
			<div class="row signal_header">
				<div class="col-md-12 signals">
					<div class="row signal">
						<div class="col-md-2">
							User
						</div>
						<div class="col-md-2">
							Registered
						</div>
						<div class="col-md-2">
							Last login
						</div>
						<div class="col-md-2">
							Scanner Expired
						</div>
						<div class="col-md-2">
							Buy Expired
						</div>
						<div class="col-md-2">
							Update
						</div>
					</div>
				</div>
			</div>
			<?php
			global $wpdb;

			$sql = 'SELECT u.ID, u.user_login, u.user_email, u.user_registered, um.meta_value date, um2.meta_value date2
				FROM wp_users u
				LEFT JOIN wp_usermeta um ON um.user_id = u.ID AND um.meta_key = "kps_cryptoscanner_expired_date"
				LEFT JOIN wp_usermeta um2 ON um2.user_id = u.ID AND um2.meta_key = "kps_cryptoscanner_buy_expired_date" ';
			
			if ($current_user_id == 8) {
				$sql .= 'WHERE u.ID NOT IN (5,6)';
			}
			
			$results = $wpdb->get_results($sql);
			foreach ($results as $key => $row) {
				$last = "";
				$when_last_login_meta = get_user_meta($row->ID, 'when_last_login',1);

				if (!empty($when_last_login_meta)) {
					$last = human_time_diff($when_last_login_meta);
				} else {
					if (get_user_meta($row->ID, 'when_last_login',1) === 0) {
						$last = __('Never', 'when-last-login');
					} else {
						update_user_meta($row->ID, 'when_last_login', 0);
						$last = __('Never', 'when-last-login');
					}
				}
				?>
				<div class="row signal_row">
					<form method="POST">
						<div class="col-md-12 signals">
							<input type="hidden" name="user_id" value="<?= $row->ID ?>">
							<div class="col-md-2">
								<span><?= $row->user_login ?></span> <br />
								<span><?= $row->user_email ?></span>
							</div>

							<div class="col-md-2">
								<?= $row->user_registered ?>
							</div>
							<div class="col-md-2">
								<?= $last ?>
							</div>

							<div class="col-md-2">
								<input type="text" name="cryptoscanner_expired_date" class="cryptoscanner_expired_date" value="<?php echo esc_attr($row->date); ?>" /><br />
							</div>
							<div class="col-md-2">
								<input type="text" name="cryptoscanner_buy_expired_date" class="cryptoscanner_buy_expired_date" value="<?php echo esc_attr($row->date2); ?>" /><br />
							</div>
							<div class="col-md-2">
								<button>Update</button>
							</div>
						</div>
					</form>	
				</div>
			<?php } ?>
		</div>

	</div>

	<?php
}

