<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
wp_register_style('cryptoscanner_css', plugin_dir_url(__FILE__) . 'assets/cryptoscanner.css');
wp_enqueue_style('cryptoscanner_css');
wp_register_script('date_js', plugin_dir_url(__FILE__) . 'assets/date.js');
wp_enqueue_script('date_js');
wp_enqueue_script('jquery-ui-datepicker');
wp_register_style('jquery-ui-style', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css?ver=1.11.4', array(), $jquery_version);
wp_enqueue_style('jquery-ui-style');
$expired_date = get_the_author_meta('kps_cryptoscanner_expired_date', get_current_user_id());
if (!check_expired_date($expired_date)) {
	?>
	<div class="container">
		<div class="alert alert-danger text-center">
			<strong>Attention!</strong> Your access to Cryptoscanner is expired <?= $expired_date?"on ".$expired_date:'' ?>
		</div>
	</div>
	<?php
} else {
	?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	jQuery(window).load(function () {
		jQuery('.signal_time').each(function(){
			var date = new Date( jQuery( this ).text()+'+00:00');
			jQuery( this ).text(date.toString('yyyy-MM-dd HH:mm:ss'));
			jQuery( this ).show();
		});
	});
	jQuery(function ($) {
		$("#startDate").datepicker({dateFormat: 'yy-mm-dd'});
		$("#endDate").datepicker({dateFormat: 'yy-mm-dd'});
	});
</script>	
<div class="container">
	<div class="row filter">
		<form method="POST">
			<div class="form-group">
				<div class="col-md-2">
					<label for="startDate">Start Date</label>
					<input type="text" name="startDate" id="startDate" value="<?= ($_POST['startDate'] ? $_POST['startDate'] : '') ?>">
				</div>
				<div class="col-md-2">	
					<label for="endDate">End Date</label>
					<input type="text" name="endDate" id="endDate" value="<?= ($_POST['endDate'] ? $_POST['endDate'] : '') ?>">
				</div>
				<div class="col-md-2">	
					<button class="filter-button" type="submit">Filter</button>
				</div>
			</div>
		</form>	
	</div>
</div>
<?php
global $wpdb;
$startDateWhere = '';
$endDateWhere = '';
if ($_POST['startDate']) {
	$startDateWhere = ' AND cs.date >= "'. $_POST['startDate'] . ' 00:00:00"';
}
if ($_POST['endDate']) {
	$endDateWhere = ' AND cs.date <= "'. $_POST['endDate'] . ' 23:59:59"';
}

$sql = 'SELECT cs.exchange, '
			. 'sum(case when status = "StopLoss" then 1 else 0 end) SLCount, '
			. 'sum(case when status = "TakeProfit" then 1 else 0 end) TPCount '
			. 'FROM wp_cryptoscanner_history cs '
			. 'WHERE cs.profit > 0 AND status != "Unknown" '
			. $startDateWhere
			. $endDateWhere
			. ' GROUP BY 1';

	$exchange_results = $wpdb->get_results($sql);
foreach ($exchange_results as $key => $row) {
	if ($row->exchange == "Bittrex") {
		$totalCountBittrex = $row->SLCount + $row->TPCount;
		$tpPercantageBittrex = number_format($row->TPCount/$totalCountBittrex*100,2);
		$statBittrex = $tpPercantageBittrex.'% ('.$row->TPCount.'/'.$totalCountBittrex.')';
	}		
	if ($row->exchange == "Binance") {		
		$totalCountBinance = $row->SLCount + $row->TPCount;
		$tpPercantageBinance = number_format($row->TPCount/$totalCountBinance*100,2);
		$statBinance = $tpPercantageBinance.'% ('.$row->TPCount.'/'.$totalCountBinance.')';
	}		
}

$sql = 'SELECT cs.exchange, sum(cs.profit) TPSum '
			. 'FROM wp_cryptoscanner_history cs '
			. 'WHERE cs.profit > 0 AND cs.status = "TakeProfit" '
			. $startDateWhere
			. $endDateWhere
			. ' GROUP BY 1';
$exchange_results2 = $wpdb->get_results($sql);
foreach ($exchange_results2 as $key => $row) {
	if ($row->exchange == "Bittrex") {
		$aveProfitBittrex = number_format($row->TPSum/$totalCountBittrex,2);
	}		
	if ($row->exchange == "Binance") {		
		$aveProfitBinance = number_format($row->TPSum/$totalCountBinance,2);
	}		
}

?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					<h1>Bittrex: <?= $statBittrex ?></h1> <?= "ave. profit " . $aveProfitBittrex . "%" ?>
				</div>
				<div class="col-md-6">
					<h1>Binance: <?= $statBinance ?></h1> <?= "ave. profit " . $aveProfitBinance . "%" ?>
				</div>
			</div>	
		</div>
	</div>	
<div class="container">
	<div id="signals">
		<div class="row signal_header">
			
			<div class="col-md-12 signals">
				<div class="row signal">
					<div class="col-md-2">
						<div class="market">Pair</div>
					</div>
					<div class="col-md-2">
						Signals given
					</div>

					<div class="col-md-2">
						Last signal time
					</div>
					<div class="col-md-2">
						Statuses
					</div>
					<div class="col-md-2">
						Stat
					</div>
<!--					<div class="col-md-1">
						Details
					</div>-->
					<div class="col-md-2">
						TP/SL %
					</div>
				</div>
			</div>
		</div>
		<?php
		
		$sql = 'SELECT cs.pair, cs.exchange, co.logo_url, csn.profit_stat, COUNT(1) qty, MAX(cs.date) date, '
				. 'sum(case when cs.status = "Active" then 1 else 0 end) ActiveCount, '
				. 'sum(case when cs.status = "StopLoss" then 1 else 0 end) SLCount, '
				. 'sum(case when cs.status = "TakeProfit" then 1 else 0 end) TPCount '
//				. 'sum(case when status = "Unknown" then 1 else 0 end) UnknownCount '
				. 'FROM wp_cryptoscanner_history cs '
				. 'LEFT JOIN wp_coins co ON co.pair = cs.pair '
				. 'LEFT JOIN wp_cryptoscanner csn ON csn.pair = cs.pair '
				. 'WHERE cs.profit > 0 AND cs.status != "Unknown" '
				. $startDateWhere
				. $endDateWhere
				. ' GROUP BY 1,2,3,4';

		$results = $wpdb->get_results($sql);
//			echo '<pre>';
//			print_r($sql);
//			echo '</pre>';
		foreach ($results as $key => $row) {
			$sql2 = 'SELECT count(cs.id) TPCount, sum(cs.profit) TPSum '
			. 'FROM wp_cryptoscanner_history cs '
			. 'WHERE cs.profit > 0 AND cs.status = "TakeProfit" '
			. 'AND cs.pair = "' . $row->pair .'"'
			. ' GROUP BY cs.pair';
			$exchange_results2 = $wpdb->get_results($sql2);

			foreach ($exchange_results2 as $key2 => $row2) {
					$aveProfit = number_format($row2->TPSum/$row2->TPCount, 2);
			}
			$sql3 = 'SELECT volume '
			. 'FROM wp_cryptoscanner '
			. 'WHERE pair = "' . $row->pair .'"';
			$exchange_results3 = $wpdb->get_results($sql3);

			foreach ($exchange_results3 as $key3 => $row3) {
					$volume = $row3->volume;
			}
			?>
			<div class="row signal_row">
				<div class="col-md-12 signals">
					<div class="row signal">
						<div class="col-md-2">
							<div class="market"><?= $row->pair ?></div>
							<div class="logo_url">
								<img id="logo_url" src="<?= $row->logo_url ?>" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<span class="qty"><?= $row->qty ?></span> <br />
							<span class="exchange"><?= $row->exchange ?></span>
						</div>
						<div class="col-md-2">
							<span class="signal_time" hidden="hidden"><?= $row->date ?></span>
						</div>
						<div class="col-md-2">
							<nobr>Active: </nobr><span class="active"><?= $row->ActiveCount ?></span> <br />
							<nobr>TakeProfit: </nobr><span class="takeprofit"><?= $row->TPCount ?></span> <br />
							<nobr>StopLoss: </nobr><span class="stoploss"><?= $row->SLCount ?></span> <br />
							<!--<nobr>Unknown: </nobr><span class="unknown"><?//= $row->UnknownCount ?></span>-->
						</div>
						<div class="col-md-2">
							<?php 
								$totalCount = $row->TPCount + $row->SLCount;
							?>	
							<nobr>Potential: </nobr><span class="profit"><?= number_format($row->TPCount/$totalCount*100,2)//$row->profit_stat ?>%<?= " (".$row->TPCount ."/". $row->SLCount.")" ?></span> <br />
							<nobr>Ave. profit: </nobr><span class="profit"><?= $aveProfit ?>%</span> <br />
							<nobr>Volume: </nobr><span class="volume"><?= number_format($volume, 3) ?></span>
						</div>
<!--						<div class="col-md-1">
						</div>-->
						<div class="col-md-2">
							<?php 
								if ($totalCount > 0 ) {
								$slPercantage = number_format($row->SLCount/$totalCount*100,2);
								$tpPercantage = number_format($row->TPCount/$totalCount*100,2);
							?>
							<span class="active"><?= $tpPercantage." / ".$slPercantage?></span> <br /> <br />
								<?php } ?>
							<a id="market_link" href="<?= 'cryptoscanner-statistic-details/?pair='.$row->pair ?>">Details</a>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>

<?php
}