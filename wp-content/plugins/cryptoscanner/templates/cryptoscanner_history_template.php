<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
wp_register_style('cryptoscanner_css', plugin_dir_url(__FILE__) . 'assets/cryptoscanner.css');
wp_enqueue_style('cryptoscanner_css');
wp_register_script('date_js', plugin_dir_url(__FILE__) . 'assets/date.js');
wp_enqueue_script('date_js');
$expired_date = get_the_author_meta('kps_cryptoscanner_expired_date', get_current_user_id());
if (!check_expired_date($expired_date)) {
	?>
	<div class="container">
		<div class="alert alert-danger text-center">
			<strong>Attention!</strong> Your access to Cryptoscanner is expired <?= $expired_date?"on ".$expired_date:'' ?>
		</div>
	</div>
	<?php
} else {
	?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	jQuery(window).load(function () {
		jQuery('.signal_time').each(function(){
			var date = new Date( jQuery( this ).text()+'+00:00');
			jQuery( this ).text(date.toString('yyyy-MM-dd HH:mm:ss'));
			jQuery( this ).show();
		});
		jQuery('.signal_closed_time').each(function(){
			var date = new Date( jQuery( this ).text()+'+00:00');
			jQuery( this ).text(date.toString('yyyy-MM-dd HH:mm:ss'));
			jQuery( this ).show();
		});
	});
</script>	
<div class="container">
	<div class="row filter">
		<form method="POST">
			<div class="form-group">
				<label class="col-md-1 checkbox-inline" for="bittrex">
					<nobr><input type="checkbox" name="exchanges[]" id="bittrex" value="Bittrex">
					Bittrex</nobr><br />
					<nobr><input type="checkbox" name="exchanges[]" id="binance" value="Binance">
					Binance</nobr>
				</label>
				<!--			<label class="col-md-1 checkbox-inline" for="binance">
								<input type="checkbox" name="exchanges" id="binance" value="Binance">
								Binance
							</label>-->
				<label class="col-md-1 checkbox-inline" for="btc">
					<nobr><input type="checkbox" name="btc" id="btc" <?= ($_POST['btc'] ? 'checked="checked"' : '') ?>>
					Bitcoin</nobr><br />
					<nobr><input type="checkbox" name="bollinger" id="bollinger" <?= ($_POST['bollinger'] ? 'checked="checked"' : '') ?>>
					Bollinger</nobr>
				</label>
				<!--			<label class="col-md-1 checkbox-inline" for="bollinger">
								<input type="checkbox" name="bollinger" id="bollinger" checked="checked" value="4">
								Bollinger
							</label>-->
				<label class="col-md-2 checkbox-inline" for="checkboxes-4">
					<nobr><input type="checkbox" name="rsi" id="rsi" <?= ($_POST['rsi'] ? 'checked="checked"' : '') ?>>
					RSI </nobr><br />
					<nobr><input type="checkbox" name="stochastic" id="stochastic" <?= ($_POST['stochastic'] ? 'checked="checked"' : '') ?>>
					Stochastic</nobr>
				</label>
				<!--			<label class="col-md-1 checkbox-inline" for="stochastic">
								<input type="checkbox" name="stochastic" id="stochastic" value="6">
								Stochastic
							</label>-->
				<div class="col-md-2">
					<label for="minProfit">MinProfit, %</label>
					<input type="number" name="minProfit" id="minProfit" value="<?= ($_POST['minProfit'] ? $_POST['minProfit'] : '1') ?>" step="0.1">
				</div>
				<div class="col-md-2">	
					<label for="minVolume">MinVolume, 24H</label>
					<input type="number" name="minVolume" id="minVolume" value="<?= ($_POST['minVolume'] ? $_POST['minVolume'] : '1') ?>" step="0.1">
				</div>
				<div class="col-md-1">	
					<label for="minVolume">Pair</label>
					<input type="text" name="pair" id="pair" value="<?= ($_POST['pair'] ? $_POST['pair'] : '') ?>">
				</div>
				<div class="col-md-2">	
					<button class="filter-button" type="submit">Filter</button>
				</div>
			</div>
		</form>	
	</div>
</div>
<div class="container">
	<div id="signals">
		<div class="row signal_header">
			<div class="col-md-1">
				<div class="market">Pair</div>
			</div>
			<div class="col-md-11 signals">
				<div class="row signal">
					<div class="col-md-2">
						Price
					</div>
					<div class="col-md-2">
						Indicators
					</div>
					<div class="col-md-3">
						Bollinger Data
					</div>
					<div class="col-md-3">
						Stat
					</div>	
					<div class="col-md-2">
						Exchange
					</div>
				</div>
			</div>
		</div>
		<?php
		global $wpdb;
		
		$limit = 25;
		if (isset($_GET["of"])) { 
			$page  = $_GET["of"]; 
		} else { 
			$page=1; 
		}  
		$start_from = ($page-1) * $limit;	
		
		
		$sql = 'SELECT cs.*, co.logo_url, csn.profit_stat FROM wp_cryptoscanner_history cs '
				. 'LEFT JOIN wp_coins co ON co.pair = cs.pair '
				. 'LEFT JOIN wp_cryptoscanner csn ON csn.pair = cs.pair '
				. 'WHERE cs.profit > 0 AND '
				. ($_POST['exchanges'] ? 'cs.exchange IN ("' . implode('","', $_POST['exchanges']) . '") AND ' : '')
				. ($_POST?($_POST['btc'] == 'on' ? 'cs.bitcoin= 1 AND ' : 'cs.bitcoin= 0 AND '):'')
				. ($_POST['bollinger'] == 'on' ? 'cs.bollinger= 1 AND ' : '')
				. ($_POST['rsi'] == 'on' ? 'cs.rsi= 1 AND ' : '')
				. ($_POST['stochastic'] == 'on' ? 'cs.stochastic= 1 AND ' : '')
				. ($_POST['pair'] ? 'cs.pair="' . $_POST['pair'] . '" AND ' : '')
				. ($_POST['minProfit'] ? 'cs.profit >= ' . $_POST['minProfit'] . ' AND ' : '')
				. ($_POST['minVolume'] ? 'cs.volume >= ' . $_POST['minVolume'] . ' AND ' : '')
				. ' 1=1 '
				. ' ORDER BY cs.ID DESC'
				. ' LIMIT '.$start_from.', '. $limit;

		$results = $wpdb->get_results($sql);
//			echo '<pre>';
//				print_r($sql);
//			echo '</pre>';
		foreach ($results as $key => $row) {
			
			$sql2 = 'SELECT count(cs.id) TPCount, sum(cs.profit) TPSum '
			. 'FROM wp_cryptoscanner_history cs '
			. 'WHERE cs.profit > 0 AND cs.status = "TakeProfit" '
			. 'AND cs.pair = "' . $row->pair .'"'
			. ' GROUP BY cs.pair';
			$exchange_results2 = $wpdb->get_results($sql2);

			foreach ($exchange_results2 as $key2 => $row2) {
					$aveProfit = number_format($row2->TPSum/$row2->TPCount, 2);
			}
			
			?>
			<div class="row signal_row">
				<div class="col-md-1">
					<div class="market"><?= $row->pair ?></div>
					<div class="logo_url">
						<img id="logo_url" src="<?= $row->logo_url ?>" alt="">
					</div>
				</div>
				<div class="col-md-11 signals">
					<div class="row signal">
						<div class="col-md-2">
							<nobr>Price: </nobr><span class="price"><?= $row->price ?></span> <br />
							<nobr>Volume: </nobr><span class="volume"><?= number_format($row->volume, 3) ?></span>
						</div>
						<div class="col-md-2">
							<nobr>Bollinger: </nobr><span class="bollinger"><?= $row->bollinger == 1 ? 'YES' : '-' ?></span> <br />
							<nobr>RSI: </nobr><span class="rsi"><?= $row->rsi == 1 ? 'YES' : '-' ?></span> <br />
							<nobr>Stochastic: </nobr><span class="stochastic"><?= $row->stochastic == 1 ? 'YES' : '-' ?></span> <br />
							<nobr>Bitcoin: </nobr><span class="bitcoin"><?= $row->bitcoin == 1 ? 'YES' : '-' ?></span>
						</div>
						<div class="col-md-3">
							<nobr>Buy: </nobr><span class="buy"><?= $row->buy ?></span> <br />
							<nobr>Sell: </nobr><span class="sell"><?= $row->sell ?></span> <br />
							<nobr>Profit: </nobr><span class="profit"><?= number_format($row->profit, 2) ?>%</span> <br />
							<nobr>Stop: </nobr><span class="stop"><?= $row->stop ?></span>
						</div>
						<div class="col-md-3">
							<nobr>Potential: </nobr><span class="profit"><a href="cryptoscanner-statistic-details/?pair=<?= $row->pair ?>"><?= $row->profit_stat ?></a></span> <br />
							<nobr>Ave. profit: </nobr><span class="aveprofit"><?= $aveProfit ?>%</span> <br />
						</div>
						<div class="col-md-2">
							<a id="market_link" href="<?= $row->link ?>" target="_blank"><?= $row->exchange ?></a> <br />
							<span class="signal_time" hidden="hidden"><?= $row->date ?></span> <br />
							<?php
								$color = "black";
								if ($row->status == 'Active') {
									$color = 'blue';
								}
								if ($row->status == 'StopLoss') {
									$color = 'red';
								}
								if ($row->status == 'TakeProfit') {
									$color = 'Green';
								}
								if ($row->status == 'Not Fulfilled') {
									$color = 'darkgoldenrod';
								}
							?>
							<span class="Status" style="font-weight: bold; color:<?= $color ?>"><?= $row->status ?></span> <br />
							<?php
								if ($row->status == 'StopLoss' || $row->status == 'TakeProfit') {
								?>		
									<span class="signal_closed_time" hidden="hidden"><?= $row->closed_date ?></span>
								<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
	<?php 
		$sql = 'SELECT COUNT(id) qty FROM wp_cryptoscanner_history cs WHERE profit > 0 AND '
				. ($_POST['exchanges'] ? 'cs.exchange IN ("' . implode('","', $_POST['exchanges']) . '") AND ' : '')
				. ($_POST?($_POST['btc'] == 'on' ? 'cs.bitcoin= 1 AND ' : 'cs.bitcoin= 0 AND '):'')
				. ($_POST['bollinger'] == 'on' ? 'cs.bollinger= 1 AND ' : '')
				. ($_POST['rsi'] == 'on' ? 'cs.rsi= 1 AND ' : '')
				. ($_POST['stochastic'] == 'on' ? 'cs.stochastic= 1 AND ' : '')
				. ($_POST['pair'] ? 'cs.pair="' . $_POST['pair'] . '" AND ' : '')
				. ($_POST['minProfit'] ? 'cs.profit >= ' . $_POST['minProfit'] . ' AND ' : '')
				. ($_POST['minVolume'] ? 'cs.volume >= ' . $_POST['minVolume'] . ' AND ' : '')
				. ' 1=1 ';  
		$rs_result = $wpdb->get_results($sql);  
		if ($_GET['of']) {
			$of=$_GET['of'];
		} else {
			$of=1;
		}
		$total_records = $rs_result[0]->qty;  
		$total_pages = ceil($total_records / $limit); 
		echo '<div class="row">'
			. '<div class="col-sm-5">'
				. '<ul class="pagination">'; 
		$pagLink = "";
		echo "<li><a href='/cryptoscanner-history/?of=1'>First</a></li>";
		if ($of <= 5) {
			$j = 1;
		} else {
			$j = $of-4;
		}
		for ($i=$j; $i<=$j+9; $i++) {  
					 $pagLink .= "<li ".($i==$of? "class='active'":"")."><a href='/cryptoscanner-history/?of=".$i."'>".$i."</a></li>";  
		} 
		echo $pagLink;
		echo "<li><a href='/cryptoscanner-history/?of=".$total_pages."'>Last</a></li>";
		echo "</ul></div>"; 
		echo '<div class="col-sm-5"><div class="pages">Pages - ' . $total_pages . '</div></div>';
		echo "</div>";
		
	?>
</div>

<?php

//	$sql2 = "SELECT * 
//			FROM (SELECT pair, COUNT(1) qty
//						FROM wp_cryptoscanner_history cs
//                  		WHERE status = 'Active'
//						GROUP BY 1) as total
//			WHERE total.qty > 1";
//			
//
//	$results2 = $wpdb->get_results($sql2);
//	
//	foreach ($results2 as $key => $value) {
//		$sql3 = "SELECT *
//					FROM wp_cryptoscanner_history cs
//					WHERE status = 'Active' AND pair = '".$value->pair."'";
//			
//
//		$results3 = $wpdb->get_results($sql3);
//		echo '<pre>';
//			print_r($results3);
//		echo '</pre>';
//		$ids = array();
//		foreach ($results3 as $key1 => $value1) {
//			$ids[] = $value1->ID;
//			$sql4 = "DELETE FROM wp_cryptoscanner_history 
//					WHERE ID NOT IN (". implode(",", $ids).")"
//					. " AND pair='".$value1->pair."'"
//					. " AND bollinger=".$value1->bollinger
//					. " AND rsi=".$value1->rsi
//					. " AND stochastic=".$value1->stochastic
//					. " AND buy=".$value1->buy
//					. " AND sell=".$value1->sell
//					. " AND stop=".$value1->stop
//					. " AND profit=".$value1->profit
//					. " AND status = 'Active'"
//					. " AND TIMESTAMPDIFF(MINUTE,'".$value1->date."',date) < 10";
//			echo '<pre>';
//			print_r($sql4);
//			echo '</pre>';
//
//			$results4 = $wpdb->get_results($sql4);
//		}
//	}

}