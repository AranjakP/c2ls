<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
wp_register_style('cryptoscanner_css', plugin_dir_url(__FILE__) . 'assets/cryptoscanner.css');
wp_enqueue_style('cryptoscanner_css');
wp_register_script('date_js', plugin_dir_url(__FILE__) . 'assets/date.js');
wp_enqueue_script('date_js');
$expired_date = get_the_author_meta('kps_cryptoscanner_expired_date', get_current_user_id());
if (!check_expired_date($expired_date)) {
	?>
	<div class="container">
		<div class="alert alert-danger text-center">
			<strong>Attention!</strong> Your access to Cryptoscanner is expired <?= $expired_date?"on ".$expired_date:'' ?>
		</div>
	</div>
	<?php
} else {
	?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	jQuery(window).load(function () {
		jQuery('.signal_time').each(function(){
			var date = new Date( jQuery( this ).text()+'+00:00');
			jQuery( this ).text(date.toString('yyyy-MM-dd HH:mm:ss'));
			jQuery( this ).show();
		});
		jQuery('.signal_closed_time').each(function(){
			var date = new Date( jQuery( this ).text()+'+00:00');
			jQuery( this ).text(date.toString('yyyy-MM-dd HH:mm:ss'));
			jQuery( this ).show();
		});
	});
</script>	
<div class="container">
	<div id="signals">
		<div class="row signal_header">
			<div class="col-md-1">
				<div class="market">Pair</div>
			</div>
			<div class="col-md-11 signals">
				<div class="row signal">
					<div class="col-md-2">
						Price
					</div>
					<div class="col-md-3">
						Stat
					</div>

					<div class="col-md-2">
						Bollinger
					</div>
					<div class="col-md-1">
						RSI
					</div>
					<div class="col-md-2">
						Stochastic
					</div>
					<div class="col-md-2">
						Exchange
					</div>
				</div>
			</div>
		</div>
		<?php
		global $wpdb;
		$sql = 'SELECT cs.*, co.logo_url FROM wp_cryptoscanner_history cs '
				. 'LEFT JOIN wp_coins co ON co.pair = cs.pair '
				. 'WHERE cs.profit > 0 AND '
//				. ($_POST['exchanges'] ? 'cs.exchange IN ("' . implode('","', explode(',', $_POST['exchanges'])) . '") AND ' : '')
//				. ($_POST['btc'] == 'on' ? 'cs.bitcoin= 1 AND ' : '')
//				. ($_POST['bollinger'] == 'on' ? 'cs.bollinger= 1 AND ' : '')
//				. ($_POST['rsi'] == 'on' ? 'cs.rsi= 1 AND ' : '')
//				. ($_POST['stochastic'] == 'on' ? 'cs.stochastic= 1 AND ' : '')
				. ($_GET['pair'] ? 'cs.pair="' . $_GET['pair'] . '" AND ' : '')
//				. ($_POST['minProfit'] ? 'cs.profit >= ' . $_POST['minProfit'] . ' AND ' : '')
//				. ($_POST['minVolume'] ? 'cs.volume >= ' . $_POST['minVolume'] . ' AND ' : '')
				. ' 1=1 '
				. ' ORDER BY cs.ID DESC';

		$results = $wpdb->get_results($sql);
//			echo '<pre>';
//			print_r($results);
//			echo '</pre>';
		foreach ($results as $key => $row) {
			?>
			<div class="row signal_row">
				<div class="col-md-1">
					<div class="market"><?= $row->pair ?></div>
					<div class="logo_url">
						<img id="logo_url" src="<?= $row->logo_url ?>" alt="">
					</div>
				</div>
				<div class="col-md-11 signals">
					<div class="row signal">
						<div class="col-md-2">
							<nobr>Price: </nobr><span class="price"><?= $row->price ?></span> <br />
							<nobr>Volume: </nobr><span class="volume"><?= number_format($row->volume, 3) ?></span>
						</div>
						<div class="col-md-3">
							<nobr>Buy: </nobr><span class="buy"><?= $row->buy ?></span> <br />
							<nobr>Sell: </nobr><span class="sell"><?= $row->sell ?></span> <br />
							<nobr>Profit: </nobr><span class="profit"><?= number_format($row->profit, 2) ?>%</span> <br />
							<nobr>Stop: </nobr><span class="stop"><?= $row->stop ?></span>
						</div>

						<div class="col-md-2">
							<span class="bollinger"><?= $row->bollinger == 1 ? 'YES' : '-' ?></span>
						</div>
						<div class="col-md-1">
							<span class="rsi"><?= $row->rsi == 1 ? 'YES' : '-' ?></span>
						</div>
						<div class="col-md-2">
							<span class="stochastic"><?= $row->stochastic == 1 ? 'YES' : '-' ?></span>
						</div>
						<div class="col-md-2">
							<a id="market_link" href="<?= $row->link ?>" target="_blank"><?= $row->exchange ?></a> <br />
							<span class="signal_time" hidden="hidden"><?= $row->date ?></span> <br />
							<?php
								$color = "black";
								if ($row->status == 'Active') {
									$color = 'blue';
								}
								if ($row->status == 'StopLoss') {
									$color = 'red';
								}
								if ($row->status == 'TakeProfit') {
									$color = 'Green';
								}
								if ($row->status == 'Not Fulfilled') {
									$color = 'darkgoldenrod';
								}
							?>
							<span class="Status" style="font-weight: bold; color:<?= $color ?>"><?= $row->status ?></span> <br />
							<?php
								if ($row->status == 'StopLoss' || $row->status == 'TakeProfit') {
								?>		
									<span class="signal_closed_time" hidden="hidden"><?= $row->closed_date ?></span>
								<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>

<?php
}