<?php
/*
  Plugin Name: Account Tabs
  Description: Extension to ultimate member
  Version: 0.1
  Author: KPS 
 */

add_filter('um_account_page_default_tabs_hook', 'referrals_tab_in_um', 100 );
function referrals_tab_in_um( $tabs ) {
	$tabs[150]['referrals']['icon'] = 'um-faicon-pencil';
	$tabs[150]['referrals']['title'] = 'Referrals program';
	$tabs[150]['referrals']['custom'] = true;
	$tabs[150]['api_keys']['icon'] = 'um-faicon-key';
	$tabs[150]['api_keys']['title'] = 'Your API Keys';
	$tabs[150]['api_keys']['custom'] = true;
	$tabs[150]['your_orders']['icon'] = 'um-faicon-shopping-cart';
	$tabs[150]['your_orders']['title'] = 'Your Orders';
	$tabs[150]['your_orders']['custom'] = true;
	$tabs[150]['premium_subscriptions']['icon'] = 'um-icon-ribbon-b';
	$tabs[150]['premium_subscriptions']['title'] = 'Premium Subscriptions';
	$tabs[150]['premium_subscriptions']['custom'] = true;
	$tabs[150]['sltp_history']['icon'] = 'um-faicon-history';
	$tabs[150]['sltp_history']['title'] = 'Your trade history';
	$tabs[150]['sltp_history']['custom'] = true;
	return $tabs;
}
	
/* make our new tab hookable */

add_action('um_account_tab__referrals', 'um_account_tab__referrals');
function um_account_tab__referrals( $info ) {
	global $ultimatemember;
	extract( $info );

	$output = $ultimatemember->account->get_tab_output('referrals');
	if ( $output ) { echo $output; }
}

add_action('um_account_tab__api_keys', 'um_account_tab__api_keys');
function um_account_tab__api_keys( $info ) {
	global $ultimatemember;
	extract( $info );

	$output = $ultimatemember->account->get_tab_output('api_keys');
	if ( $output ) { echo $output; }
}

add_action('um_account_tab__your_orders', 'um_account_tab__your_orders');
function um_account_tab__your_orders( $info ) {
	global $ultimatemember;
	extract( $info );

	$output = $ultimatemember->account->get_tab_output('your_orders');
	if ( $output ) { echo $output; }
}

add_action('um_account_tab__premium_subscriptions', 'um_account_tab__premium_subscriptions');
function um_account_tab__premium_subscriptions( $info ) {
	global $ultimatemember;
	extract( $info );

	$output = $ultimatemember->account->get_tab_output('premium_subscriptions');
	if ( $output ) { echo $output; }
}

add_action('um_account_tab__sltp_history', 'um_account_tab__sltp_history');
function um_account_tab__sltp_history( $info ) {
	global $ultimatemember;
	extract( $info );

	$output = $ultimatemember->account->get_tab_output('sltp_history');
	if ( $output ) { echo $output; }
}

/* Finally we add some content in the tab */

add_filter('um_account_content_hook_referrals', 'um_account_content_hook_referrals');
function um_account_content_hook_referrals( $output ){
	ob_start();
	$gen = new Gens_RAF_Public('gens-raf', '1.1.4');
	$referralID = $gen->get_referral_id(get_current_user_id());
	$refLink = esc_url(add_query_arg( 'raf', $referralID, get_home_url() )); 
	
	global $wpdb;
	$sql = 'SELECT COUNT(DISTINCT(user_id)) as refs FROM wp_usermeta WHERE meta_key ="_raf_id" AND meta_value="'.$referralID.'"';
	$results = $wpdb->get_results($sql);
	
	$rewards = get_user_meta(get_current_user_id(), '_rewards_amount', 1);

	?>

	<div class="um-field">

		<div id="raf-message" class=""><?php _e( 'Your Referral URL:','gens-raf'); ?> <a id="a_ref_url" href="<?php echo $refLink; ?>" ><?php echo $refLink; ?></a></div>
		<button id="copy_url">Copy Url</button>
	</div>	
	<div class="um-field">

		<div class=""><?php _e( 'Your Referral Code: ','gens-raf'); ?><span id="raf-code"><?php echo $referralID; ?></span></div>
		<button id="copy_code">Copy Code</button>
		
	</div>
	<div class="um-field">

		<div id="raf-count" class=""><?php _e( 'Number of your referrals: ','gens-raf');?><?php echo $results[0]->refs ?></div>
		
	</div>	
	
	<?php
	
	if ($rewards) {
		foreach ($rewards as $key => $value) {
			?>
				<div class="um-field">

					<div  class=""><?php _e( 'Rewards amount: ','gens-raf');?><?php echo $value." ".$key ?></div>
		
				</div>
			<?php
		}
	} else {
		?>
	
			<div class="um-field">

				<div  class=""><?php _e( 'You have no rewards.','gens-raf');?></div>

			</div>
		<?php
	}
	?>
		<script type="text/javascript">
			jQuery("#copy_code").click(function(event) {
				event.preventDefault();
				var $temp = jQuery("<input>");
				jQuery("body").append($temp);
				$temp.val(jQuery('#raf-code').text()).select();
				document.execCommand("copy");
				$temp.remove();
			});
			jQuery("#copy_url").click(function(event) {
				event.preventDefault();
				var $temp = jQuery("<input>");
				jQuery("body").append($temp);
				$temp.val(jQuery('#a_ref_url').attr("href")).select();
				document.execCommand("copy");
				$temp.remove();
			});
		</script>	
	<?php

//	$order = new WC_Order(164);
//	$user_id = $order->get_user_id();
//	foreach ($order->get_items() as $item) {
//		if ('line_item' == $item['type']) {
//			$product = new WC_Product_Variable($item->get_product_id());
//			$Subscription_key = $product->get_attribute('Subscription_key');
//			if ($Subscription_key) {
//				echo '<pre>';
//				print_r($Subscription_key);
//				echo '</pre>';
//				$variation = new WC_Product_Variation( $item->get_variation_id() );
//				$Subscription = $variation->get_attribute('Subscription');
//				echo '<pre>';
//				print_r($Subscription);
//				echo '</pre>';
//			}
//		}
//	}
	$output .= ob_get_contents();
	ob_end_clean();
	return $output;
}

add_filter('um_account_content_hook_api_keys', 'um_account_content_hook_api_keys');
function um_account_content_hook_api_keys( $output ){
	ob_start();

//	$exmo_api_key = get_user_meta(get_current_user_id(), 'exmo_api_key', 1);
	$bittrex_api_key = get_user_meta(get_current_user_id(), 'bittrex_api_key', 1);
	$bittrex_api_secret = get_user_meta(get_current_user_id(), 'bittrex_api_secret', 1);
	$binance_api_key = get_user_meta(get_current_user_id(), 'binance_api_key', 1);
	$binance_api_secret = get_user_meta(get_current_user_id(), 'binance_api_secret', 1);

	?>
	<div class="um-account-heading uimob340-hide uimob500-hide">
		<i class="um-faicon-key"></i>Your API Keys
	</div>
		
<!--	<div class="um-field">

		<div class="um-field-label">
			<label for="first_name">Exmo API Key</label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<input autocomplete="off" class="um-form-field valid " type="text" name="exmo_api_key" id="exmo_api_key" value="<?=$exmo_api_key?>" placeholder="" data-validate="" data-key="exmo_api_key">
		</div>
		
	</div>	-->
	
	<div class="um-field">
		<div class="um-account-heading uimob340-hide uimob500-hide">
			Bittrex
		</div>
		<div class="ex_wrap">
			<div class="um-field-label">
				<label for="first_name">Key</label>
				<div class="um-clear"></div>
			</div>
			<div class="um-field-area">
				<input autocomplete="off" class="um-form-field valid " type="text" name="bittrex_api_key" id="bittrex_api_key" value="<?=$bittrex_api_key?>" placeholder="" data-validate="" data-key="bittrex_api_key">
			</div>
			<div class="um-field-label">
				<label for="first_name">Secret</label>
				<div class="um-clear"></div>
			</div>
			<div class="um-field-area">
				<input autocomplete="off" class="um-form-field valid " type="text" name="bittrex_api_secret" id="bittrex_api_secret" value="<?=$bittrex_api_secret?>" placeholder="" data-validate="" data-key="bittrex_api_secret">
			</div>
		</div>	
	</div>
	<div class="um-field">
		<div class="um-account-heading uimob340-hide uimob500-hide">
			Binance
		</div>
		<div class="ex_wrap">
			<div class="um-field-label">
				<label for="first_name">Key</label>
				<div class="um-clear"></div>
			</div>
			<div class="um-field-area">
				<input autocomplete="off" class="um-form-field valid " type="text" name="binance_api_key" id="binance_api_key" value="<?= $binance_api_key ?>" placeholder="" data-validate="" data-key="binance_api_key">
			</div>
			<div class="um-field-label">
				<label for="first_name">Secret</label>
				<div class="um-clear"></div>
			</div>
			<div class="um-field-area">
				<input autocomplete="off" class="um-form-field valid " type="text" name="binance_api_secret" id="binance_api_secret" value="<?= $binance_api_secret ?>" placeholder="" data-validate="" data-key="binance_api_secret">
			</div>
		</div>	
	</div>
	<div class="um-col-alt um-col-alt-b">
			<div class="um-left">
				<input type="submit" name="um_account_submit" id="um_account_submit" value="Update Keys" class="um-button">
			</div>
		<div class="um-clear"></div>
	</div>

	<?php
	$output .= ob_get_contents();
	ob_end_clean();
	return $output;
}

add_filter('um_account_content_hook_premium_subscriptions', 'um_account_content_hook_premium_subscriptions');
function um_account_content_hook_premium_subscriptions( $output ){
	ob_start();

	$kps_exmosltp_expired_date = get_user_meta(get_current_user_id(), 'kps_exmosltp_expired_date', 1);
//	$kps_btrxfastbuy_expired_date = get_user_meta(get_current_user_id(), 'kps_btrxfastbuy_expired_date', 1);
	$kps_cryptoscanner_expired_date = get_user_meta(get_current_user_id(), 'kps_cryptoscanner_expired_date', 1);
	$kps_bittrexsltp_expired_date = get_user_meta(get_current_user_id(), 'kps_bittrexsltp_expired_date', 1);
	?>
	<div class="um-account-heading uimob340-hide uimob500-hide">
		<i class="um-icon-ribbon-b"></i>Premium Subscriptions
	</div>
		
	<div class="um-field">

		<div class="um-field-label">
			<label for="kps_exmosltp_expired_date">Subscription for Exmo StopLoss / TakeProfit application up to</label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<input disabled="disabled" autocomplete="off" class="um-form-field valid " type="text" id="kps_exmosltp_expired_date" value="<?=$kps_exmosltp_expired_date?>">
		</div>
		
	</div>	
	<div class="um-field">

		<div class="um-field-label">
			<label for="kps_bittrexsltp_expired_date">Subscription for Bittrex StopLoss / TakeProfit application up to</label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<input disabled="disabled" autocomplete="off" class="um-form-field valid " type="text" id="kps_bittrexsltp_expired_date" value="<?=$kps_bittrexsltp_expired_date?>">
		</div>
		
	</div>		
<!--	<div class="um-field">

		<div class="um-field-label">
			<label for="kps_btrxfastbuy_expired_date">Subscription for Bittrex Fast Buy/Sell application up to</label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<input disabled="disabled" autocomplete="off" class="um-form-field valid " type="text" id="bittrex_api_key" value="<?//=$kps_btrxfastbuy_expired_date?>">
		</div>
		
	</div>-->
	<div class="um-field">

		<div class="um-field-label">
			<label for="kps_cryptoscanner_expired_date">Subscription for Cryptoscanner</label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<input disabled="disabled" autocomplete="off" class="um-form-field valid " type="text" id="bittrex_api_key" value="<?=$kps_cryptoscanner_expired_date?>">
		</div>
		
	</div>	

	<?php
	$output .= ob_get_contents();
	ob_end_clean();
	return $output;
}

add_filter('um_account_content_hook_your_orders', 'um_account_content_hook_your_orders');
function um_account_content_hook_your_orders( $output ){
	ob_start();

	?>
	<div class="um-account-heading uimob340-hide uimob500-hide">
		<i class="um-faicon-shopping-cart"></i>Your Orders
	</div>
	<?php		
	$my_orders_columns = array(
		'order-number'  => __( 'Order', 'woocommerce' ),
		'order-date'    => __( 'Date', 'woocommerce' ),
		'order-status'  => __( 'Status', 'woocommerce' ),
		'order-total'   => __( 'Total', 'woocommerce' ),
	);

	$customer_orders = get_posts( array(
		'numberposts' => -1,
		'meta_key'    => '_customer_user',
		'meta_value'  => get_current_user_id(),
		'post_type'   => wc_get_order_types( 'view-orders' ),
		'post_status' => array_keys( wc_get_order_statuses() ),
	) );

	if ( $customer_orders ) : ?>

		<table class="shop_table shop_table_responsive my_account_orders" style="margin-top: 20px;">

			<thead>
				<tr>
					<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
						<th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
					<?php endforeach; ?>
				</tr>
			</thead>

			<tbody>
				<?php foreach ( $customer_orders as $customer_order ) :
					$order      = wc_get_order( $customer_order );
					$item_count = $order->get_item_count();
					?>
					<tr class="order">
						<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
							<td class="<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
								<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
									<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

								<?php elseif ( 'order-number' === $column_id ) : ?>
									<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
										<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?>
									</a>

								<?php elseif ( 'order-date' === $column_id ) : ?>
									<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

								<?php elseif ( 'order-status' === $column_id ) : ?>
									<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

								<?php elseif ( 'order-total' === $column_id ) : ?>
									<?php
									/* translators: 1: formatted order total 2: total order items */
									printf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count );
									?>
								<?php endif; ?>
							</td>
						<?php endforeach; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php else : ?>
	<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info"  style="margin-top: 20px;">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php _e( 'Go shop', 'woocommerce' ) ?>
		</a>
		<?php _e( 'No order has been made yet.', 'woocommerce' ); ?>
	</div>
	<?php endif;

	$output .= ob_get_contents();
	ob_end_clean();
	return $output;
}

add_filter('um_account_content_hook_sltp_history', 'um_account_content_hook_sltp_history');
function um_account_content_hook_sltp_history( $output ){
	ob_start();

	?>
	<div class="um-account-heading uimob340-hide uimob500-hide">
		<i class="um-faicon-history"></i>Your trade history
	</div>
		
	<?php		
	$my_orders_columns = array(
		'date'    => __( 'Date', 'woocommerce' ),
		'pair'  => __( 'Pair', 'woocommerce' ),		
		'action'  => __( 'Action', 'woocommerce' ),
		'settings'   => __( 'Settings', 'woocommerce' ),
	);
	global $wpdb;
	$sql = 'SELECT * FROM wp_exmosltp_statistic WHERE user_id="'.get_current_user_id().'"';
	$results = $wpdb->get_results($sql);

	if (sizeof($results) > 0 ) : ?>

		<div style="margin-top:20px;">Exmo Exchange:</div>
		<table class="shop_table shop_table_responsive my_account_orders" style="margin-top: 10px;">

			<thead>
				<tr>
					<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
						<th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
					<?php endforeach; ?>
				</tr>
			</thead>

			<tbody>
				<?php foreach ( $results as $row => $value ) :

					?>
					<tr class="order">
						<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
							<td class="<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
								<?php if ( 'date' === $column_id ) : 
										echo esc_html($value->date); 
									elseif ( 'pair' === $column_id ) : 
										echo esc_html($value->pair); 
									elseif ( 'action' === $column_id ) : 
										echo esc_html($value->action); 
									elseif ( 'settings' === $column_id ) :
										echo "<ul>";
										foreach (maybe_unserialize($value->settings) as $key => $value) {
											echo "<li>".$key." ".$value."</li>";
										}
										echo "</ul>";
										//echo esc_html($value->settings); 
									endif; 
								?>
							</td>
						<?php endforeach; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php else : ?>
	<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info"  style="margin-top: 20px;">
		<?php _e( 'You have not trade history yet.', 'woocommerce' ); ?>
	</div>
	<?php endif;
	
	$output .= ob_get_contents();
	ob_end_clean();
	return $output;
}