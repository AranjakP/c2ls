<?php

/*
  Controller name: ExmoSLTP
  Controller description: ExmoSLTP functions
 */

class JSON_API_ExmoSLTP_Controller {

	public function sltp() {
		global $json_api;

		if (get_option('kps_exmosltp_enable_client_access')) {

			if ($json_api->query->version != '0.7') {
				$json_api->error('', 'er01');
			}

			$userkey = $json_api->query->key;
			$users = get_users(array('meta_key' => 'gens_referral_id', 'meta_value' => $userkey));

			if (empty($users)) {
//				$access_user_not_found_text = get_option('kps_exmosltp_access_user_not_found_text');
//				$json_api->error($access_user_not_found_text, 'er02');
				$premium = "no";
				$user_id = 4;
			} else {
				$premium = "yes";
				$user_id = $users[0]->data->ID;
			}
			
			$expired_date = get_the_author_meta('kps_exmosltp_expired_date', $user_id);
			if (!$this->check_expired_date($expired_date)) {
				$premium = "no";
			}

			$this->kps_save_client_data_to_db($json_api->query->key, $json_api->query->version, $user_id, $json_api->query->lang);

			$message_to_clients = get_option('kps_exmosltp_message_to_clients');
			return array(
				"message_to_clients" => $message_to_clients,
				"expired_date" => $expired_date,
				"premium" => $premium,
			);
		} else {
			$json_api->error(get_option('kps_exmosltp_access_denied_text'), 'er05');
		}
	}

	private function kps_save_client_data_to_db($key, $version, $user_id, $lang) {
		global $wpdb;

		$sql2 = "INSERT INTO `wp_exmosltp_clients`(`exmo_key`,`version`,`user_id`,`lang`) VALUES ('$key','$version','$user_id','$lang')";
		$wpdb->get_results($sql2);
	}

	private function check_expired_date($expired_date) {
		$expired_datetime = strtotime($expired_date);
		if ($expired_datetime < time()) {
			return false;
		}
		return true;
	}

	public function sltp_mail_done() {
		global $json_api;

		if (get_option('kps_exmosltp_enable_client_access')) {
			
			$userkey = $json_api->query->key;
			$users = get_users(array('meta_key' => 'gens_referral_id', 'meta_value' => $userkey));
			if (empty($users)) {
//				$access_user_not_found_text = get_option('kps_exmosltp_access_user_not_found_text');
//				$json_api->error($access_user_not_found_text, 'er02');
				$user_id = 4;
			} else {
				$user_id = $users[0]->data->ID;
			}
			
			$pair = $json_api->query->pair;
			$price = $json_api->query->price;
			$lang = $json_api->query->lang;
			$sltp_action = $json_api->query->sltp_action;
			$settings['sl_price'] = $json_api->query->sl_price;
			$settings['sl_qty'] = $json_api->query->sl_qty;
			$settings['sl_if'] = $json_api->query->sl_if;
			$settings['tp_price'] = $json_api->query->tp_price;
			$settings['tp_qty'] = $json_api->query->tp_qty;
			$settings['tp_if'] = $json_api->query->tp_if;
			$expired_date = get_the_author_meta('kps_exmosltp_expired_date', $user_id);
			if ($this->check_expired_date($expired_date)) {
				$to = $users[0]->data->user_email;
				if ($lang == 'EN') {
					$subject = 'Exmo ' . $sltp_action . ' - for pair ' . $pair . ' has been created order with price ' . $price;
					$body = 'Hi there!<br>Best vishes, ' . get_bloginfo('name') . '!';
				} else if ($lang == 'UA') {
					$subject = 'Exmo ' . $sltp_action . ' - для пари ' . $pair . ' створено ордер з ціною ' . $price;
					$body = 'Доброго дня!<br>З найкращими побажаннями, ' . get_bloginfo('name') . '!';
				} else {
					$subject = 'Exmo ' . $sltp_action . ' - для пары ' . $pair . ' создан ордер с ценой ' . $price;
					$body = 'Добрый день!<br>С наилучшими пожеланиями, ' . get_bloginfo('name') . '!';
				}
				$headers = array('Content-Type: text/html; charset=UTF-8; From: ' . get_bloginfo('name') . ';');

				wp_mail($to, $subject, $body, $headers);
			}
			
			global $wpdb;

			$sql2 = "INSERT INTO `wp_exmosltp_statistic`(`user_id`,`pair`,`action`,`settings`) VALUES ('" . $user_id . "','" . $pair . "','" . $sltp_action . "','" . serialize($settings) . "')";
			$wpdb->get_results($sql2);

			return array(
				"expired_date" => $expired_date
			);
		} else {
			$json_api->error(get_option('kps_exmosltp_access_denied_text'), 'er05');
		}
	}
	
	public function sltp_mail_trailing_activated() {
		global $json_api;

		if (get_option('kps_exmosltp_enable_client_access')) {
			
			$userkey = $json_api->query->key;
			$users = get_users(array('meta_key' => 'gens_referral_id', 'meta_value' => $userkey));
			if (empty($users)) {
//				$access_user_not_found_text = get_option('kps_exmosltp_access_user_not_found_text');
//				$json_api->error($access_user_not_found_text, 'er02');
				$user_id = 4;
			} else {
				$user_id = $users[0]->data->ID;
			}
			
			$pair = $json_api->query->pair;
			$lang = $json_api->query->lang;
			$expired_date = get_the_author_meta('kps_exmosltp_expired_date', $user_id);
			if ($this->check_expired_date($expired_date)) {
				$to = $users[0]->data->user_email;
				if ($lang == 'EN') {
					$subject = 'Exmo Trailing TakeProfit - for pair ' . $pair . '  has been activated.';
					$body = 'Hi there!<br>Best vishes, ' . get_bloginfo('name') . '!';
				} else if ($lang == 'UA') {
					$subject = 'Exmo Trailing TakeProfit - для пари ' . $pair . ' було активовано.';
					$body = 'Доброго дня!<br>З найкращими побажаннями, ' . get_bloginfo('name') . '!';
				} else {
					$subject = 'Exmo Trailing TakeProfit - для пары ' . $pair . ' был активирован.';
					$body = 'Добрый день!<br>С наилучшими пожеланиями, ' . get_bloginfo('name') . '!';
				}
				$headers = array('Content-Type: text/html; charset=UTF-8; From: ' . get_bloginfo('name') . ';');

				wp_mail($to, $subject, $body, $headers);
			}

			return array(
				"expired_date" => $expired_date
			);
		} else {
			$json_api->error(get_option('kps_exmosltp_access_denied_text'), 'er05');
		}
	}

}

?>
