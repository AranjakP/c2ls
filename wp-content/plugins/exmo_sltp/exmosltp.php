<?php
/*
  Plugin Name: ExmoSLTP
  Description: Controller for ExmoSLTP Application
  Version: 0.1
  Author: KPS 
 */

add_action('admin_menu', 'c2ls_menu');

function c2ls_menu() {
	add_menu_page('ExmoSLTP', 'ExmoSLTP', 'manage_options', 'kps_c2ls', 'kps_c2ls_page', 'dashicons-clipboard');
}

function kps_c2ls_page() {
	?>
	<div class="wrap">
		<form action="options.php" method="post" style="margin-left: 20px;">

			<?php
			settings_fields('kps_exmosltp_settings');
			do_settings_sections("kps_exmosltp_page");
			submit_button('Save Settings', 'primary', 'save_kps_exmosltp', true);
			kps_get_exmosltp_statistic();
                        kps_get_bittrexsltp_statistic();
			?>
		</form>
	</div>
	<?php
}

add_action('admin_init', function() {
	add_settings_section(
			'kps_exmosltp_settings_section', // ID used to identify this section and with which to register options
			'ExmoSLTP Settings', // Title to be displayed on the administration page
			'kps_exmosltp_settings_callback', // Callback used to render the description of the section
			'kps_exmosltp_page' // Page on which to add this section of options
	);

	add_settings_field(
			'kps_exmosltp_enable_client_access', 'Enable access for clients', 'kps_exmosltp_checkbox_callback', 'kps_exmosltp_page', 'kps_exmosltp_settings_section', ['label_for' => 'kps_enable_client_access', 'option' => 'kps_exmosltp_enable_client_access']
	);

	add_settings_field(
			'kps_exmosltp_access_user_not_found_text', 'Text for User not found', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_exmosltp_settings_section', ['description' => '', 'label_for' => 'kps_access_user_not_found_text', 'option' => 'kps_exmosltp_access_user_not_found_text', 'placeholder' => 'User not found']
	);

	add_settings_field(
			'kps_exmosltp_access_denied_text', 'Text for Access Denied', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_exmosltp_settings_section', ['description' => '', 'label_for' => 'kps_access_denied_text', 'option' => 'kps_exmosltp_access_denied_text', 'placeholder' => 'Access Denied Text']
	);

	add_settings_field(
			'kps_exmosltp_access_expired_text', 'Text for Access Expired', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_exmosltp_settings_section', ['description' => '', 'label_for' => 'kps_access_expired_text', 'option' => 'kps_exmosltp_access_expired_text', 'placeholder' => 'Access Expired Text']
	);

	add_settings_field(
			'kps_exmosltp_message_to_clients', 'Message to clients', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_exmosltp_settings_section', ['description' => '', 'label_for' => 'kps_message_to_clients', 'option' => 'kps_exmosltp_message_to_clients', 'placeholder' => 'Message to clients']
	);
		
	register_setting('kps_exmosltp_settings', 'kps_exmosltp_enable_client_access');
	register_setting('kps_exmosltp_settings', 'kps_exmosltp_access_user_not_found_text');
	register_setting('kps_exmosltp_settings', 'kps_exmosltp_access_denied_text');
	register_setting('kps_exmosltp_settings', 'kps_exmosltp_access_expired_text');
	register_setting('kps_exmosltp_settings', 'kps_exmosltp_message_to_clients');	
        
        add_settings_section(
			'kps_bittrexsltp_settings_section', // ID used to identify this section and with which to register options
			'BittrexSLTP Settings', // Title to be displayed on the administration page
			'kps_exmosltp_settings_callback', // Callback used to render the description of the section
			'kps_exmosltp_page' // Page on which to add this section of options
	);

	add_settings_field(
			'kps_bittrexsltp_enable_client_access', 'Enable access for clients', 'kps_exmosltp_checkbox_callback', 'kps_exmosltp_page', 'kps_bittrexsltp_settings_section', ['label_for' => 'kps_enable_client_access', 'option' => 'kps_bittrexsltp_enable_client_access']
	);

	add_settings_field(
			'kps_bittrexsltp_access_user_not_found_text', 'Text for User not found', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_bittrexsltp_settings_section', ['description' => '', 'label_for' => 'kps_access_user_not_found_text', 'option' => 'kps_bittrexsltp_access_user_not_found_text', 'placeholder' => 'User not found']
	);

	add_settings_field(
			'kps_bittrexsltp_access_denied_text', 'Text for Access Denied', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_bittrexsltp_settings_section', ['description' => '', 'label_for' => 'kps_access_denied_text', 'option' => 'kps_bittrexsltp_access_denied_text', 'placeholder' => 'Access Denied Text']
	);

	add_settings_field(
			'kps_bittrexsltp_access_expired_text', 'Text for Access Expired', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_bittrexsltp_settings_section', ['description' => '', 'label_for' => 'kps_access_expired_text', 'option' => 'kps_bittrexsltp_access_expired_text', 'placeholder' => 'Access Expired Text']
	);

	add_settings_field(
			'kps_bittrexsltp_message_to_clients', 'Message to clients', 'kps_exmosltp_text_area_callback', 'kps_exmosltp_page', 'kps_bittrexsltp_settings_section', ['description' => '', 'label_for' => 'kps_message_to_clients', 'option' => 'kps_bittrexsltp_message_to_clients', 'placeholder' => 'Message to clients']
	);
		
	register_setting('kps_exmosltp_settings', 'kps_bittrexsltp_enable_client_access');
	register_setting('kps_exmosltp_settings', 'kps_bittrexsltp_access_user_not_found_text');
	register_setting('kps_exmosltp_settings', 'kps_bittrexsltp_access_denied_text');
	register_setting('kps_exmosltp_settings', 'kps_bittrexsltp_access_expired_text');
	register_setting('kps_exmosltp_settings', 'kps_bittrexsltp_message_to_clients');	
});

function kps_exmosltp_settings_callback() {

}

function kps_exmosltp_checkbox_callback($args) {
	$option = get_option($args['option']);
	?>
	<input type="checkbox" name="<?= $args['option'] ?>" <?php echo esc_attr($option == 'on' ? 'checked="checked"' : ''); ?>/>
	<?php
}

function kps_exmosltp_input_callback($args) {
	$option = get_option($args['option']);
	?>
	<input type="text" placeholder="<?= $args['placeholder'] ?>" name="<?= $args['option'] ?>" value="<?php echo esc_attr($option); ?>" size="50" />
	<?php
}

function kps_exmosltp_input_number_callback($args) {
	$option = get_option($args['option']);
	?>
	<input type="text" min="10" name="<?= $args['option'] ?>" value="<?php echo esc_attr($option); ?>"/>
	<?php
}

function kps_exmosltp_text_area_callback($args) {
	$option = get_option($args['option']);
	?>
	<textarea placeholder="<?= $args['placeholder'] ?>" rows="3" cols="55" name="<?= $args['option'] ?>" /><?php echo esc_attr($option); ?></textarea>
	<?php
}

function kps_get_exmosltp_statistic() {
	global $wpdb;
	$sql = "SELECT COUNT(DISTINCT exmo_key) as users FROM wp_exmosltp_clients WHERE date >= (NOW() - INTERVAL 600 SECOND)";
	$results = $wpdb->get_results($sql);
	
	echo "Exmo Users online: ".$results[0]->users."</br>";
//	echo "Sum in action: ".$results[0]->sum."</br>";
//	echo "Users balance: ".$results[0]->balance."</br>";
	
}

function kps_get_bittrexsltp_statistic() {
	global $wpdb;
	$sql = "SELECT COUNT(DISTINCT bittrex_key) as users FROM wp_bittrexsltp_clients WHERE date >= (NOW() - INTERVAL 600 SECOND)";
	$results = $wpdb->get_results($sql);
	
	echo "Bittrex Users online: ".$results[0]->users."</br>";
//	echo "Sum in action: ".$results[0]->sum."</br>";
//	echo "Users balance: ".$results[0]->balance."</br>";
	
}

/*********************************************************************/

function kps_add_exmosltp_controller($controllers) {
	$controllers[] = 'exmosltp';
        $controllers[] = 'bittrexsltp';
	return $controllers;
}

add_filter('json_api_controllers', 'kps_add_exmosltp_controller');

function kps_set_exmosltp_controller_path() {
	return plugin_dir_path(__FILE__) . "exmosltp_controller.php";
}

add_filter('json_api_exmosltp_controller_path', 'kps_set_exmosltp_controller_path');

function kps_set_bittrexsltp_controller_path() {
	return plugin_dir_path(__FILE__) . "bittrexsltp_controller.php";
}

add_filter('json_api_bittrexsltp_controller_path', 'kps_set_bittrexsltp_controller_path');

add_action('show_user_profile', 'kps_sltp_profile_fields');
add_action('edit_user_profile', 'kps_sltp_profile_fields');

function kps_sltp_profile_fields($user) {
	wp_enqueue_script( 'jquery-ui-datepicker' );
	?>
	<h3><?php _e("SLTP information", "blank"); ?></h3>
	<script>
	jQuery(function($) {
		$( "#kps_exmosltp_expired_date" ).datepicker({ dateFormat: 'MM d, yy' });
        $( "#kps_bittrexsltp_expired_date" ).datepicker({ dateFormat: 'MM d, yy' });
	});
	</script>
	<table class="form-table">
		<tr>
			<th><label for="expired_date"><?php _e("Exmo Expired Date"); ?></label></th>
			<td>
				<input type="text" name="kps_exmosltp_expired_date" id="kps_exmosltp_expired_date" value="<?php echo esc_attr(get_the_author_meta('kps_exmosltp_expired_date', $user->ID)); ?>" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="expired_date"><?php _e("Bittrex Expired Date"); ?></label></th>
			<td>
				<input type="text" name="kps_bittrexsltp_expired_date" id="kps_bittrexsltp_expired_date" value="<?php echo esc_attr(get_the_author_meta('kps_bittrexsltp_expired_date', $user->ID)); ?>" /><br />
			</td>
		</tr>
	</table>
	<h3><?php _e("Cryptoscanner", "blank"); ?></h3>
	<script>
	jQuery(function($) {
        $( "#kps_cryptoscanner_expired_date" ).datepicker({ dateFormat: 'MM d, yy' });
	});
	</script>
	<table class="form-table">
		<tr>
			<th><label for="expired_date"><?php _e("Cryptoscanner Expired Date"); ?></label></th>
			<td>
				<input type="text" name="kps_cryptoscanner_expired_date" id="kps_cryptoscanner_expired_date" value="<?php echo esc_attr(get_the_author_meta('kps_cryptoscanner_expired_date', $user->ID)); ?>" /><br />
			</td>
		</tr>
	</table>
<?php
}

add_action('personal_options_update', 'kps_save_sltp_profile_fields');
add_action('edit_user_profile_update', 'kps_save_sltp_profile_fields');

function kps_save_sltp_profile_fields($user_id) {
	if (!current_user_can('edit_user', $user_id)) {
		return false;
	}
	update_user_meta($user_id, 'kps_exmosltp_expired_date', $_POST['kps_exmosltp_expired_date']);
	update_user_meta($user_id, 'kps_exmosltp_expired_datetime', strtotime($_POST['kps_exmosltp_expired_date']));
	update_user_meta($user_id, 'kps_bittrexsltp_expired_date', $_POST['kps_bittrexsltp_expired_date']);
	update_user_meta($user_id, 'kps_bittrexsltp_expired_datetime', strtotime($_POST['kps_bittrexsltp_expired_date']));
	update_user_meta($user_id, 'kps_cryptoscanner_expired_date', $_POST['kps_cryptoscanner_expired_date']);
	update_user_meta($user_id, 'kps_cryptoscanner_expired_datetime', strtotime($_POST['kps_cryptoscanner_expired_date']));
}