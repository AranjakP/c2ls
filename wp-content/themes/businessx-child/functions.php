<?php
/* ------------------------------------------------------------------------- *
 *  Functions
 *  ________________
 *
 *	For more info on Child Themes:
 *	http://codex.wordpress.org/Child_Themes
 *	________________
 *
/* ------------------------------------------------------------------------- */


// Get Parent Styles
function businessx_child_theme_enqueue_styles() {
	
	// CSS
    $parent_style = 'businessx-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'businessx-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
	
}
add_action( 'wp_enqueue_scripts', 'businessx_child_theme_enqueue_styles' );


// JS
function businessx_child_theme_enqueue_scripts() {
	wp_enqueue_script( 'businessx-child-scripts', get_stylesheet_directory_uri() . '/assets/js/child-scripts.js', array( 'jquery' ), '20160412', true );
}
add_action( 'wp_enqueue_scripts', 'businessx_child_theme_enqueue_scripts', 99 );



// Google Maps Api
function bx_google_maps_creds() {
	return 'YOUR API KEY GOES HERE';
}

add_action("um_before_form", "redirect_to_account", 10, 1);

function redirect_to_account($args) {
	if (is_user_logged_in() && $args['mode']=='register') {
		wp_redirect('/account');
	}
}

add_filter('jetpack_social_media_icons_widget_array', 'kps_add_telegram_to_social', 10, 1);

function kps_add_telegram_to_social($html) {
	$html[] = '<a href="https://t.me/joinchat/FG5tzxAe4vnIosnfzKdY_A" class="genericon" target="_blank"><span class="screen-reader-text">Join to cryptocurrency_tools’s Telegram chat</span><svg class="genericon genericond-social-logos social-logos-telegram genericond-social-logos-1x genericond-rotate-normal" style="fill:inherit;" title="telegram" role="img" aria-labelledby="title"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://cc2ls.io/wp-content/plugins/genericond//icons/social-logos/svg-sprite/social-logos.svg#telegram"></use></svg></a>';
			
	return $html;		
}