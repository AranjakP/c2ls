<?php
/**
 * Основные параметры WordPress.
 *
* Скрипт для создания wp-config.php использует этот файл в процессе * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'c2ls');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'qwe012');

/** Имя сервера MySQL */
define('DB_HOST', '192.168.1.8');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%<qk+`|Jd%K;!;gcjIg9a9d8NFzWF+4|g%OS6d~Z#m#>0#|p1m~9}e=mXCE*Wihe');
define('SECURE_AUTH_KEY',  'cHbYa:G?%YE+*i]2:z78GquLbq_jxYz-M&9Q8_L(:r%k&<Ev<V{&yRJ|)tC)p#9Y');
define('LOGGED_IN_KEY',    'km)^q:].+,_&1E_jGr1(<w ]|[peHUkhK].ZV|qSw#_r,a k$UV`daB%2O$}![.)');
define('NONCE_KEY',        '[AInBi*_P)eRT6LNC2IP7|hwV4K|G94(jbH`Z.&{2|u-}GL~}~Wkhd@Uo<}}4a&2');
define('AUTH_SALT',        'O+3cu8@;@83v|]!~)eH|w@ck^A_dmHE`%jzPwN&;u^jr*:xn39n<W(e-`p,Gs9k.');
define('SECURE_AUTH_SALT', '<2whqcWBP20xtV==ddZ!Z>Eq#j5Ce({,!h4Nq[.N+.}zv0X@4d~D m1,-KG1Q-dA');
define('LOGGED_IN_SALT',   'zj9A&|-e.()x-s$3Wy7w`y-<7g|C$TMHu<B&BpfW @p^T/KvNG5>OMJbz Wm8F0:');
define('NONCE_SALT',       'vfBoet6gK);R.z1uHFfkxqtwW0iLVrF/b20/+8O(./0FBnBg!}8ZhXlYy1!_#ULQ');
define('FS_METHOD', 'direct');
/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
